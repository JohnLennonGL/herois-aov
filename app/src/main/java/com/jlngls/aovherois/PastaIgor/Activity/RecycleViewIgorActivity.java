package com.jlngls.aovherois.PastaIgor.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jlngls.aovherois.PastaIgor.Adapter.AdapterIgor;
import com.jlngls.aovherois.PastaJohn.Adapter.AdapterJohn;
import com.jlngls.aovherois.Model.ModelLista;
import com.jlngls.aovherois.R;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewIgorActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView alterarNome;
    private List<ModelLista> heroisLista = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycle_view);

        recyclerView = findViewById(R.id.recyclerViewID);
        alterarNome = findViewById(R.id.NomeJogador);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        ListaAddHerois();
        RecyclerView.Adapter adapter = new AdapterIgor(this, heroisLista);
        recyclerView.setAdapter(adapter);

        alterarNome.setText("Igor(VBREscanor1v9)");

    }
    public void sair(View view){
        finish();
    }
    private void ListaAddHerois(){
        ModelLista postar = new ModelLista("Florentino", "Lendario", "Guerreiro/Assassino", R.drawable.florentino);
        heroisLista.add(postar);

        postar = new ModelLista("Violet", "Lendario", "Atirador", R.drawable.violet);
        heroisLista.add(postar);

        postar = new ModelLista("Annete", "Mestre", "Mago/Suporte", R.drawable.annete);
        heroisLista.add(postar);

        postar = new ModelLista("Amily", "Mestre", "Guerreiro", R.drawable.amily);
        heroisLista.add(postar);

        postar = new ModelLista("Riktor", "Mestre", "Guerreiro/Assassino", R.drawable.riktor);
        heroisLista.add(postar);

        postar = new ModelLista("veres", "Mestre", "Guerreiro", R.drawable.veres);
        heroisLista.add(postar);

        postar = new ModelLista("Khali", "Experiente", "Mago", R.drawable.khali);
        heroisLista.add(postar);


    }
    }

