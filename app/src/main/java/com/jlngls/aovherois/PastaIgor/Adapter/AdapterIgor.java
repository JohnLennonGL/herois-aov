package com.jlngls.aovherois.PastaIgor.Adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.aovherois.Model.ModelLista;
import com.jlngls.aovherois.PastaJohn.Adapter.AdapterJohn;
import com.jlngls.aovherois.R;

import java.util.List;
public class AdapterIgor extends RecyclerView.Adapter<AdapterIgor.IgorViewHolder> {

    private List<ModelLista> heroislista;
    private Context cxt;

    public AdapterIgor(Context context, List lista) {

        heroislista = lista;
        cxt = context;
    }

    @NonNull
    @Override
    public IgorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View lista = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_igor, parent, false);

        return new AdapterIgor.IgorViewHolder(lista);
    }

    @Override
    public void onBindViewHolder(@NonNull final IgorViewHolder holder, int position) {
        final ModelLista postagem = heroislista.get(position);
        holder.nomeHeroi.setText(postagem.heroi);
        holder.experenciaMOD.setText(postagem.experiencia);
        holder.classeMOD.setText(postagem.classe);
        holder.imagemMOD.setImageResource(postagem.imagem);
        holder.seekBarMOD.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.facil3));
                } else if (progress == 1) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.meio4));

                } else if (progress == 2) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.medio5));

                } else if (progress == 3) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.medio6));

                } else if (progress == 4) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.dificil7));

                } else if (progress == 5) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.dificil8));

                } else if (progress == 6) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.hard9));

                } else if (progress == 7) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.hard10));

                } else {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.fullhard12));


                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return heroislista.size();
    }

    public  class IgorViewHolder extends RecyclerView.ViewHolder{

        private TextView nomeHeroi, experenciaMOD, classeMOD;
        private ImageView imagemMOD, imagemEmojis;
        private SeekBar seekBarMOD;
        private EditText partidas;
        private RatingBar ratingBar;

        public IgorViewHolder(@NonNull View itemView) {
            super(itemView);
            nomeHeroi = itemView.findViewById(R.id.heroiNomeID2);
            experenciaMOD = itemView.findViewById(R.id.experenciaID2);
            classeMOD = itemView.findViewById(R.id.classeID2);
            seekBarMOD = itemView.findViewById(R.id.seekBarID2);
            imagemEmojis = itemView.findViewById(R.id.emojisID2);
            partidas = itemView.findViewById(R.id.editTextpartidas2);
            imagemMOD = itemView.findViewById(R.id.imagemID2);
        }
    }
}
