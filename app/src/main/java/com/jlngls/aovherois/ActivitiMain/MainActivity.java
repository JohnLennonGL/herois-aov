package com.jlngls.aovherois.ActivitiMain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jlngls.aovherois.PastaIgor.Activity.RecycleViewIgorActivity;
import com.jlngls.aovherois.PastaJohn.Activity.MainRecycleView;
import com.jlngls.aovherois.R;

public class MainActivity extends AppCompatActivity {
private TextView nomeJogador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        nomeJogador = findViewById(R.id.NomeJogador);
    }



    public void onClickisuku(View view){
        startActivity(new Intent(MainActivity.this, MainRecycleView.class));
    }

    public void onClickEscanor(View view){
        startActivity(new Intent(MainActivity.this, RecycleViewIgorActivity.class));

    }
}
