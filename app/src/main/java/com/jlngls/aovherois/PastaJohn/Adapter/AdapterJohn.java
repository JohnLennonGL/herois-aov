package com.jlngls.aovherois.PastaJohn.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.aovherois.Model.ModelLista;
import com.jlngls.aovherois.R;

import java.util.List;

public class AdapterJohn extends RecyclerView.Adapter<AdapterJohn.MyViewHolder> {

    private List<ModelLista> heroislista;
    private Context cxt;


    public AdapterJohn(Context context, List lista) {

        heroislista = lista;
        cxt = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View lista = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_john, parent, false);

        return new MyViewHolder(lista);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final ModelLista postagem = heroislista.get(position);
        holder.nomeHeroi.setText(postagem.heroi);
        holder.experenciaMOD.setText(postagem.experiencia);
        holder.classeMOD.setText(postagem.classe);
        holder.imagemMOD.setImageResource(postagem.imagem);
        holder.seekBarMOD.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.facil3));
                } else if (progress == 1) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.meio4));

                } else if (progress == 2) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.medio5));

                } else if (progress == 3) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.medio6));

                } else if (progress == 4) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.dificil7));

                } else if (progress == 5) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.dificil8));

                } else if (progress == 6) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.hard9));

                } else if (progress == 7) {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.hard10));

                } else {
                    holder.imagemEmojis.setImageDrawable(ContextCompat.getDrawable(cxt, R.drawable.fullhard12));


                }




            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    @Override
    public int getItemCount() {
        return heroislista.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView nomeHeroi, experenciaMOD, classeMOD;
        private ImageView imagemMOD, imagemEmojis;
        private SeekBar seekBarMOD;
        private EditText partidas, winrat;
        private RatingBar ratingBar;
        private static final String AQUIVO_PREFERENCIA = "Arquivo Preferencia";
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);


            nomeHeroi = itemView.findViewById(R.id.heroiNomeID);
            experenciaMOD = itemView.findViewById(R.id.experenciaID);
            classeMOD = itemView.findViewById(R.id.classeID);
            imagemMOD = itemView.findViewById(R.id.imagemID);
            seekBarMOD = itemView.findViewById(R.id.seekBarID);
            imagemEmojis = itemView.findViewById(R.id.emojisID);
            partidas = itemView.findViewById(R.id.editTextpartidas);
            winrat = itemView.findViewById(R.id.editTextWR);
            ratingBar = itemView.findViewById(R.id.ratingBar);

        }
    }
}