package com.jlngls.aovherois.PastaJohn.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jlngls.aovherois.PastaJohn.Adapter.AdapterJohn;
import com.jlngls.aovherois.Model.ModelLista;
import com.jlngls.aovherois.R;

import java.util.ArrayList;
import java.util.List;

public class MainRecycleView extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView alterarNome;
    private List<ModelLista> heroisLista = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycle_view);

        recyclerView = findViewById(R.id.recyclerViewID);

        // RecycleView
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        //Adapter
        ListaAddHerois();
        RecyclerView.Adapter adapter = new AdapterJohn(this, heroisLista);
        recyclerView.setAdapter(adapter);

        // Nome do Jogador
        alterarNome = findViewById(R.id.NomeJogador);
        alterarNome.setText("John(VBRxIsuku)");




    }

        private void ListaAddHerois(){
            ModelLista postar = new ModelLista("Arthur", "Lendario", "Tanque/Guerreiro", R.drawable.arthur);
            heroisLista.add(postar);

            postar = new ModelLista("Diaochan", "Lendario", "Mago", R.drawable.diaochan);
            heroisLista.add(postar);

            postar = new ModelLista("TelAnnas", "Lendario", "Atirador", R.drawable.telannass);
            heroisLista.add(postar);

            postar = new ModelLista("Amily", "Lendario", "Guerreiro", R.drawable.amily);
            heroisLista.add(postar);

            postar = new ModelLista("Hayate", "Lendario", "Atirador", R.drawable.hayate);
            heroisLista.add(postar);

            postar = new ModelLista("Gildur", "Lendario", "Mago/Guerreiro", R.drawable.gildur);
            heroisLista.add(postar);

            postar = new ModelLista("Raz", "Lendario", "Mago", R.drawable.raz);
            heroisLista.add(postar);

            postar = new ModelLista("Yorn", "Lendario", "Atirador", R.drawable.yorn);
            heroisLista.add(postar);
    }

    //Botao Voltar
    public void sair(View view){
        finish();}

}
